port module Main exposing (Model, Msg(..), init, main, update, view)

import Browser
import Debug exposing (log)
import EncodeableUrl exposing (EncodeDomain(..), decodeEncodeableUrl, encodeEncodableUrl)
import Html exposing (Html, a, button, div, footer, form, h1, input, label, li, option, p, select, text, ul)
import Html.Attributes exposing (attribute, class, href, placeholder, target, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import Http
import Json.Decode exposing (Decoder, bool, decodeString, field, int, list, map2, map4, string)
import Json.Encode as Encode



---- MODEL ----


type alias Link =
    { break : String
    , id : Int
    , title : String
    , url : String
    }


type alias NewLink =
    { break : String
    , title : String
    , url : String
    }


linkDecoder : Decoder Link
linkDecoder =
    map4 Link
        (field "break" string)
        (field "id" int)
        (field "title" string)
        (field "url" string)


suggestionDecoder : Decoder String
suggestionDecoder =
    field "suggestion" string


type alias InitState =
    { host : String
    , apiKey : String
    }


initStateDecoder : Decoder InitState
initStateDecoder =
    map2 InitState
        (field "host" string)
        (field "apiKey" string)


type alias FetchResponce =
    { message : String
    , error : Bool
    }


fetchResponceDecoder : Decoder FetchResponce
fetchResponceDecoder =
    map2 FetchResponce
        (field "message" string)
        (field "error" bool)


type ViewMode
    = Today
    | Backlog
    | New
    | Edit
    | Search
    | Settings


type NotificationType
    = Error
    | Success
    | Pending


type alias Notification =
    { text : String
    , kind : NotificationType
    }


type alias Model =
    { mode : ViewMode
    , host : String
    , apiKey : String
    , todayLinks : List Link
    , backlogLinks : List Link
    , title : String
    , url : String
    , break : String
    , id : Maybe Int
    , notification : Maybe Notification
    , searchModel : SearchModel
    }


init : String -> ( Model, Cmd Msg )
init flags =
    let
        res =
            case decodeString initStateDecoder flags of
                Ok r ->
                    r

                Err errorMessage ->
                    { host = "", apiKey = "" }
    in
    ( { mode = Today
      , host = res.host
      , apiKey = res.apiKey
      , todayLinks = []
      , backlogLinks = []
      , title = ""
      , url = ""
      , break = ""
      , id = Nothing
      , notification = Nothing
      , searchModel = initSearchModel flags
      }
    , getLinks TodayRequest res.host res.apiKey
    )


type LinkRequest
    = BacklogRequest
    | TodayRequest


getLinks : LinkRequest -> String -> String -> Cmd Msg
getLinks t host apiKey =
    let
        cb =
            if t == TodayRequest then
                GotTodayLinks

            else
                GotBacklogLinks

        url =
            if t == TodayRequest then
                "/api/links/todays"

            else
                "/api/links/backlog"
    in
    Http.request
        { body = Http.emptyBody
        , expect = Http.expectString cb
        , headers = [ Http.header "Accept" "application/json", Http.header "Authorization" ("key " ++ apiKey) ]
        , method = "GET"
        , timeout = Nothing
        , tracker = Nothing
        , url = host ++ url
        }


viewedLink : Int -> String -> String -> Cmd Msg
viewedLink id host apiKey =
    let
        url =
            "/api/links/view/" ++ String.fromInt id
    in
    Http.request
        { body = Http.emptyBody
        , expect = Http.expectString DidLinkAction
        , headers = [ Http.header "Accept" "application/json", Http.header "Authorization" ("key " ++ apiKey) ]
        , method = "GET"
        , timeout = Nothing
        , tracker = Nothing
        , url = host ++ url
        }


deleteLink : Int -> String -> String -> Cmd Msg
deleteLink id host apiKey =
    let
        url =
            "/api/links/delete/" ++ String.fromInt id
    in
    Http.request
        { body = Http.emptyBody
        , expect = Http.expectString DidLinkAction
        , headers = [ Http.header "Accept" "application/json", Http.header "Authorization" ("key " ++ apiKey) ]
        , method = "GET"
        , timeout = Nothing
        , tracker = Nothing
        , url = host ++ url
        }


createLink : NewLink -> String -> String -> Cmd Msg
createLink link host apiKey =
    let
        json =
            Encode.object
                [ ( "connection"
                  , Encode.object
                        [ ( "url", Encode.string (host ++ "/api/links") )
                        , ( "apiKey", Encode.string apiKey )
                        ]
                  )
                , ( "link"
                  , Encode.object
                        [ ( "title", Encode.string link.title )
                        , ( "url", Encode.string link.url )
                        , ( "break", Encode.string link.break )
                        ]
                  )
                ]
    in
    saveNewLinkPort json


updateLink : Link -> String -> String -> Cmd Msg
updateLink link host apiKey =
    let
        json =
            Encode.object
                [ ( "connection"
                  , Encode.object
                        [ ( "url", Encode.string (host ++ "/api/links/" ++ String.fromInt link.id) )
                        , ( "apiKey", Encode.string apiKey )
                        ]
                  )
                , ( "link"
                  , Encode.object
                        [ ( "title", Encode.string link.title )
                        , ( "url", Encode.string link.url )
                        , ( "break", Encode.string link.break )
                        , ( "id", Encode.int link.id )
                        ]
                  )
                ]
    in
    saveNewLinkPort json


getTitleSuggestion : String -> String -> String -> Cmd Msg
getTitleSuggestion url host apiKey =
    Http.request
        { body = Http.emptyBody
        , expect = Http.expectString GotTitleSuggestion
        , headers = [ Http.header "Accept" "application/json", Http.header "Authorization" ("key " ++ apiKey) ]
        , method = "GET"
        , timeout = Nothing
        , tracker = Nothing
        , url = host ++ "/api/suggest?url=" ++ url
        }


handleEncodedUrl : Model -> String -> ( Model, Cmd Msg )
handleEncodedUrl model val =
    case decodeString decodeEncodeableUrl val of
        Ok res ->
            case res.encodeDomain of
                SearchEncodeDomain ->
                    ( model, getSearchResult res.url model.apiKey model.host )

                LinkFormDomain ->
                    ( { model | url = res.url }, getTitleSuggestion res.url model.host model.apiKey )

        Err errorMessage ->
            ( { model | notification = Just { text = "Error Decoding EncodeableUrl", kind = Error } }, Cmd.none )



---- UPDATE ----


type Msg
    = SearchParentMsg SearchMsg
    | SwitchMode ViewMode
    | SetHost String
    | SetApiKey String
    | SaveSettings
    | SetTitle String
    | SetUrl String
    | SetBreak String
    | SaveLink
    | ViewLink Int
    | DeleteLink Int
    | EditLink Link
    | ClearLinkForm
    | DidNewLinkSave String
    | DidLinkAction (Result Http.Error String)
    | GotTitleSuggestion (Result Http.Error String)
    | DidEncodeUrlPort String
    | GotTodayLinks (Result Http.Error String)
    | GotBacklogLinks (Result Http.Error String)



-- = NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SearchParentMsg m2 ->
            let
                ( sModel, cmd ) =
                    handleSearchMsg m2 model.searchModel model.apiKey model.host
            in
            ( { model | searchModel = sModel }, cmd )

        SwitchMode newMode ->
            case newMode of
                Backlog ->
                    ( { model | mode = newMode }, getLinks BacklogRequest model.host model.apiKey )

                Today ->
                    ( { model | mode = newMode }, getLinks TodayRequest model.host model.apiKey )

                _ ->
                    ( { model | mode = newMode }, Cmd.none )

        SetHost host ->
            ( { model | host = host }, Cmd.none )

        SetApiKey key ->
            ( { model | apiKey = key }, Cmd.none )

        SaveSettings ->
            ( model
            , saveSettingsPort
                (Encode.object
                    [ ( "host", Encode.string model.host )
                    , ( "apiKey", Encode.string model.apiKey )
                    ]
                )
            )

        SetTitle title ->
            ( { model | title = title }, Cmd.none )

        SetUrl url ->
            ( { model | url = url }, encodeUrlPort (encodeEncodableUrl { encodeDomain = LinkFormDomain, url = url }) )

        SetBreak brk ->
            ( { model | break = brk }, Cmd.none )

        ViewLink id ->
            ( model, viewedLink id model.host model.apiKey )

        DeleteLink id ->
            ( model, deleteLink id model.host model.apiKey )

        SaveLink ->
            case model.id of
                Nothing ->
                    ( { model | notification = Just { text = "Saving new link", kind = Pending } }
                    , createLink
                        { title = model.title
                        , url = model.url
                        , break = model.break
                        }
                        model.host
                        model.apiKey
                    )

                Just id ->
                    ( { model | notification = Just { text = "Updating existing link", kind = Pending } }
                    , updateLink
                        { title = model.title
                        , url = model.url
                        , break = model.break
                        , id = id
                        }
                        model.host
                        model.apiKey
                    )

        EditLink link ->
            ( { model
                | title = link.title
                , url = link.url
                , break = link.break
                , id = Just link.id
                , mode = Edit

                -- , id = Maybe.withDefault (Just 0) (Just link.id)
              }
            , Cmd.none
            )

        ClearLinkForm ->
            ( { model
                | title = ""
                , url = ""
                , break = ""
                , id = Nothing
                , mode = New
              }
            , Cmd.none
            )

        DidNewLinkSave data ->
            case decodeString fetchResponceDecoder data of
                Ok res ->
                    if res.error then
                        ( { model | notification = Just { text = res.message, kind = Error } }, Cmd.none )

                    else
                        ( { model
                            | notification = Just { text = res.message, kind = Success }
                            , title = ""
                            , url = ""
                            , break = ""
                            , id = Nothing
                          }
                        , Cmd.none
                        )

                Err errorMessage ->
                    ( { model | notification = Just { text = "Error Decoding Fetch Responce", kind = Error } }, Cmd.none )

        -- ( model, Cmd.none )
        GotTodayLinks (Ok x) ->
            case decodeString (field "data" (list linkDecoder)) x of
                Ok links ->
                    ( { model | todayLinks = links }, Cmd.none )

                Err errorMessage ->
                    ( model, Cmd.none )

        GotTodayLinks (Err _) ->
            ( model, Cmd.none )

        GotBacklogLinks (Ok x) ->
            case decodeString (field "data" (list linkDecoder)) x of
                Ok links ->
                    ( { model | backlogLinks = links }, Cmd.none )

                Err errorMessage ->
                    ( model, Cmd.none )

        GotBacklogLinks (Err _) ->
            ( model, Cmd.none )

        GotTitleSuggestion (Ok x) ->
            case decodeString suggestionDecoder x of
                Ok title ->
                    ( { model | title = title }, Cmd.none )

                Err errorMessage ->
                    ( model, Cmd.none )

        GotTitleSuggestion (Err _) ->
            ( model, Cmd.none )

        DidEncodeUrlPort val ->
            handleEncodedUrl model val

        DidLinkAction (Ok x) ->
            case model.mode of
                Backlog ->
                    ( model, getLinks BacklogRequest model.host model.apiKey )

                Today ->
                    ( model, getLinks TodayRequest model.host model.apiKey )

                _ ->
                    ( model, Cmd.none )

        DidLinkAction (Err _) ->
            ( { model | notification = Just { text = "Error in link action", kind = Error } }, Cmd.none )



---- VIEW ----


viewLinkForm : Model -> Html Msg
viewLinkForm model =
    div [ class "section" ]
        [ div [ class "field" ]
            [ label [ class "label" ] [ text "Title:" ]
            , div [ class "control" ]
                [ input [ type_ "text", class "input", placeholder "title", value model.title, onInput SetTitle ] [] ]
            ]
        , div [ class "field" ]
            [ label [ class "label" ] [ text "Url:" ]
            , div [ class "control" ]
                [ input [ type_ "text", class "input", placeholder "url", value model.url, onInput SetUrl ] [] ]
            ]
        , div [ class "field" ]
            [ label [ class "label" ] [ text "Break:" ]
            , div [ class "control" ]
                [ input [ type_ "text", class "input", placeholder "break", value model.break, onInput SetBreak ] [] ]
            ]
        , div [ class "field" ]
            [ div [ class "control" ]
                [ button [ class "button is-link", onClick SaveLink ] [ text "Save" ]
                , button [ class "button is-link", onClick ClearLinkForm ] [ text "Clear" ]
                ]
            ]
        ]


viewSettings : Model -> Html Msg
viewSettings model =
    div [ class "section" ]
        [ div [ class "field" ]
            [ label [ class "label" ] [ text "Host:" ]
            , div [ class "control" ]
                [ input [ type_ "text", class "input", placeholder "host", value model.host, onInput SetHost ] [] ]
            ]
        , div [ class "field" ]
            [ label [ class "label" ] [ text "API Key:" ]
            , div [ class "control" ]
                [ input [ type_ "text", class "input", placeholder "apikey", value model.apiKey, onInput SetApiKey ] [] ]
            ]
        , div [ class "field" ]
            [ div [ class "control" ]
                [ button [ class "button is-link", onClick SaveSettings ] [ text "Save" ] ]
            ]
        ]


viewSearch : Model -> Html Msg
viewSearch model =
    div [ class "section" ]
        [ div [ class "field" ]
            [ label [ class "label" ] [ text "Search:" ]
            , div [ class "control" ]
                [ input [ type_ "text", class "input", placeholder "query", value model.searchModel.query, onInput (\str -> SearchParentMsg (SearchUpdateQuery str)) ] [] ]
            ]
        , label [ class "label" ] [ text "Links:" ]
        , div [] (List.map viewLink model.searchModel.links)
        ]


viewLink : Link -> Html Msg
viewLink link =
    div [ class "box" ]
        [ div [ class "columns is-mobile" ]
            [ p [ class "column is-one-fifth" ] [ text link.break ]
            , a [ class "column", href link.url, target "_blank" ] [ text link.title ]
            ]
        , div [ class "container link__wrapper" ]
            [ button [ class "button", onClick (EditLink link) ] [ text "edit" ]
            , button [ class "button", onClick (DeleteLink link.id) ] [ text "delete" ]
            , button [ class "button", onClick (ViewLink link.id) ] [ text "view" ]
            ]
        ]


switchViewModeWithString : String -> Msg
switchViewModeWithString str =
    case str of
        "today" ->
            SwitchMode Today

        "backlog" ->
            SwitchMode Backlog

        _ ->
            SwitchMode Today


viewLinks : Model -> Html Msg
viewLinks model =
    div [ class "section" ]
        [ div [ class "field" ]
            [ label [ class "label" ] [ text "Type of Link:" ]
            , div [ class "control" ]
                [ div [ class "select" ]
                    [ select [ onInput switchViewModeWithString ]
                        [ option [ value "today" ] [ text "Today" ]
                        , option [ value "backlog" ] [ text "Backlog" ]
                        ]
                    ]
                ]
            ]
        , div []
            (case model.mode of
                Today ->
                    List.map viewLink model.todayLinks

                Backlog ->
                    List.map viewLink model.backlogLinks

                _ ->
                    List.map viewLink model.todayLinks
            )
        ]


viewContent : Model -> Html Msg
viewContent model =
    case model.mode of
        Settings ->
            viewSettings model

        Today ->
            viewLinks model

        Backlog ->
            viewLinks model

        Search ->
            viewSearch model

        New ->
            viewLinkForm model

        Edit ->
            viewLinkForm model


notificationColorHelper : NotificationType -> String
notificationColorHelper t =
    case t of
        Error ->
            "has-text-white has-background-danger"

        Pending ->
            "has-text-white has-background-info"

        Success ->
            "has-text-white has-background-success"


viewNotifications : Model -> Html Msg
viewNotifications model =
    case model.notification of
        Just note ->
            div [ class ("notification" ++ " " ++ notificationColorHelper note.kind) ] [ text note.text ]

        Nothing ->
            div [] []


isActiveHelper : Bool -> String
isActiveHelper b =
    if b then
        "is-active"

    else
        ""


view : Model -> Html Msg
view model =
    div [ class "main__wrapper" ]
        [ div [ class "tabs is-centered is-boxed" ]
            [ ul []
                [ li
                    [ class (isActiveHelper (model.mode == Today || model.mode == Backlog))
                    , onClick (SwitchMode Today)
                    ]
                    [ a [] [ text "Links" ] ]
                , li
                    [ class (isActiveHelper (model.mode == New || model.mode == Edit))
                    , onClick (SwitchMode New)
                    ]
                    [ a []
                        [ text
                            (if model.id == Nothing then
                                "New"

                             else
                                "Edit"
                            )
                        ]
                    ]
                , li
                    [ class (isActiveHelper (model.mode == Search))
                    , onClick (SwitchMode Search)
                    ]
                    [ a [] [ text "Search" ] ]
                , li
                    [ class (isActiveHelper (model.mode == Settings))
                    , onClick (SwitchMode Settings)
                    ]
                    [ a [] [ text "Settings" ] ]
                ]
            ]
        , div [ class "container" ] [ viewNotifications model ]
        , div [ class "content main" ] [ viewContent model ]
        , footer [ class "has-background-light" ]
            [ div [ class "content has-text-centered" ]
                [ p [] [ text "Hypert-Web v0.2.0" ]
                ]
            ]
        ]



---- SUBSCRIPTIONS ----
-- subscriptions : Model -> Sub Msg
-- subscriptions model =
--   Time.every 1000 Tick


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ didNewLinkSavePort DidNewLinkSave
        , didEncodeUrlPort DidEncodeUrlPort
        ]



---- PORTS ----


port saveSettingsPort : Encode.Value -> Cmd msg


port saveNewLinkPort : Encode.Value -> Cmd msg


port didNewLinkSavePort : (String -> msg) -> Sub msg


port encodeUrlPort : Encode.Value -> Cmd msg


port didEncodeUrlPort : (String -> msg) -> Sub msg



---- PROGRAM ----


main : Program String Model Msg
main =
    Browser.element
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }



---- SEARCH ----
-- Search Model


type alias SearchModel =
    { query : String
    , isSearching : Bool
    , links : List Link
    }


initSearchModel : String -> SearchModel
initSearchModel flags =
    { query = ""
    , isSearching = False
    , links = []
    }



-- Search Update


type SearchMsg
    = SearchUpdateQuery String
    | SearchRecivedSearchResults (Result Http.Error String)


handleSearchMsg : SearchMsg -> SearchModel -> String -> String -> ( SearchModel, Cmd Msg )
handleSearchMsg msg model apiKey host =
    case msg of
        SearchUpdateQuery q ->
            if String.length model.query > 2 then
                ( { model | query = q }, encodeUrlPort (encodeEncodableUrl { encodeDomain = SearchEncodeDomain, url = host ++ "/api/search/links?term=" ++ q }) )

            else
                ( { model | query = q }, Cmd.none )

        SearchRecivedSearchResults (Ok x) ->
            case decodeString (field "data" (list linkDecoder)) x of
                Ok links ->
                    ( { model | links = links }, Cmd.none )

                Err errorMessage ->
                    ( model, Cmd.none )

        SearchRecivedSearchResults (Err _) ->
            ( model, Cmd.none )


getSearchResult : String -> String -> String -> Cmd Msg
getSearchResult url apiKey host =
    Http.request
        { body = Http.emptyBody
        , expect = Http.expectString (\val -> SearchParentMsg (SearchRecivedSearchResults val))
        , headers = [ Http.header "Accept" "application/json", Http.header "Authorization" ("key " ++ apiKey) ]
        , method = "GET"
        , timeout = Nothing
        , tracker = Nothing
        , url = url
        }
