import './main.css';
import { Elm } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';

var cache = localStorage.getItem('settingCache')

var app = Elm.Main.init({
  node: document.getElementById('root'),
  flags: cache ? cache : ""
});

app.ports.encodeUrlPort.subscribe(function(encodeableUrl) {
  const newUrl = encodeURI(encodeableUrl.url);
  const encodedUrl = {  ...encodeableUrl, url: newUrl };
  const json = JSON.stringify(encodedUrl);
  app.ports.didEncodeUrlPort.send(json);
});


app.ports.saveSettingsPort.subscribe(function(data) {
  console.log({data});
  localStorage.setItem('settingCache', JSON.stringify(data));
});

app.ports.saveNewLinkPort.subscribe(function(data) {
  console.log({data});
  const url = data.connection.host;
  return fetch(data.connection.url, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    headers: new Headers({
      'Authorization': 'key ' + data.connection.apiKey,
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }),
    body: JSON.stringify({link: data.link}),
  })
    .then(response => {
      return response.json();
    })
    .then(json => {
      if(!!json.errors) throw new Error(json.errors.break[0]);
      console.log({json});
      let res = {
        message: !!data.link.id ? "Link updated" : "Saved new link",
        error: false,
      };
      console.log({res});
      app.ports.didNewLinkSavePort.send(JSON.stringify(res));
    })
  .catch(e => {
      console.log({e});
      let res = {
        message: e.message,
        error: true,
      };
      console.log({res});
      app.ports.didNewLinkSavePort.send(JSON.stringify(res));
  });
});

// curl -i -XPOST -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'Authorization: key xxxx' -d '{"link":{"url":"test", "title":"Update test 2", "break":"3y", "id":37}}'  'http://0.0.0.0:4000/api/links/37'

registerServiceWorker();
