port module EncodeableUrl exposing (EncodeDomain(..), decodeEncodeableUrl, encodeEncodableUrl)

import Json.Decode exposing (Decoder, bool, decodeString, field, int, list, map2, map4, string)
import Json.Encode as Encode


type EncodeDomain
    = SearchEncodeDomain
    | LinkFormDomain


type alias EncodeableUrl =
    { encodeDomain : EncodeDomain
    , url : String
    }



---- JSON ----


encodeEncodeDomain : EncodeDomain -> Encode.Value
encodeEncodeDomain e =
    case e of
        SearchEncodeDomain ->
            Encode.string "S.E.D"

        LinkFormDomain ->
            Encode.string "L.F.D"


encodeEncodableUrl : EncodeableUrl -> Encode.Value
encodeEncodableUrl val =
    Encode.object
        [ ( "encodeDomain", encodeEncodeDomain val.encodeDomain )
        , ( "url", Encode.string val.url )
        ]


decodeEncodeableUrl : Decoder EncodeableUrl
decodeEncodeableUrl =
    map2 EncodeableUrl
        (field "encodeDomain" decodeEncodeDomain)
        (field "url" string)


decodeEncodeDomain : Decoder EncodeDomain
decodeEncodeDomain =
    Json.Decode.string
        |> Json.Decode.andThen
            (\str ->
                case str of
                    "S.E.D" ->
                        Json.Decode.succeed SearchEncodeDomain

                    "L.F.D" ->
                        Json.Decode.succeed LinkFormDomain

                    somethingElse ->
                        Json.Decode.fail <| "Unknown EncodableDomain: " ++ somethingElse
            )
